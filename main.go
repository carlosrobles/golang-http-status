package main

import (
	"encoding/json"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
)

var logger *logrus.Logger

type ReplyMessage struct {
	Message string `json:"message"`
}

func lowercase_header_keys(headers http.Header) http.Header {
	for key, value := range headers {
		if _, found := headers[strings.ToLower(key)]; !found {
			headers[strings.ToLower(key)] = value
			delete(headers, key)
		}
	}
	return headers
}

func reply_request(w http.ResponseWriter, message string, statusCode int) {
	response := ReplyMessage{Message: message}
	// we skip the json marshal error checking since
	// we are providing the input to serialize and we
	// don't expect issues with http.StatusText() output
	responseJSON, _ := json.Marshal(response)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	w.Write(responseJSON)
}

func return_requested_status_code(w http.ResponseWriter, r *http.Request) {
	uri := r.RequestURI
	headers := lowercase_header_keys(r.Header)

	logger.WithFields(logrus.Fields{
		"uri": uri,
	}).Info("Request received")

	logger.WithFields(logrus.Fields{
		"headers": headers,
	}).Info("Headers received")

	status_code, found := headers["status_code"]

	if !found {
		reply_request(w, "Please provide a status_code header", http.StatusBadRequest)
	} else {
		match, err := regexp.MatchString(`^\d{3}$`, status_code[0])
		if err != nil {
			logger.WithFields(logrus.Fields{
				"error": err,
			}).Error("An error occurred attempting to match the regex of the provided status code")
			reply_request(w, "An error occurred attempting to match the regex of the provided status code", http.StatusBadRequest)
			return
		}
		if !match {
			reply_request(w, "Please provide a three digit status_code", http.StatusBadRequest)
			return
		}

		// we skip the integer cast error check since we've validated it's 3 digits
		status_code_requested, _ := strconv.Atoi(status_code[0])
		status_text := http.StatusText(status_code_requested)
		if len(status_text) == 0 {
			reply_request(w, "Please provide a valid status_code", http.StatusBadRequest)
		} else {
			logger.WithFields(logrus.Fields{
				"status_code": status_code_requested,
			}).Info("Status code requested")
			reply_request(w, status_text, status_code_requested)
		}
	}
}

func main() {
	logger = logrus.New()
	logger.Formatter = &logrus.JSONFormatter{}
	logger.Out = os.Stdout
	logger.Level = logrus.InfoLevel

	http.HandleFunc("/status", return_requested_status_code)
	http.ListenAndServe(":8081", nil)
}
