down:
	docker-compose down

build: down
	docker-compose build status-robles-io

test: build-test
	docker-compose run test

build-test:
	docker-compose build test

up: build
	docker-compose up -d

.PHONY: down build up test build-test
