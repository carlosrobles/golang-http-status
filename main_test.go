package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"strconv"
	"testing"

	"github.com/sirupsen/logrus"
)

var statusCodes = []int{100, 101, 102, 103, 200, 201, 202, 203, 204, 205, 206, 207, 208, 226, 300, 301, 302, 303, 304, 305, 307, 308, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 421, 422, 423, 424, 425, 426, 428, 429, 431, 451, 500, 501, 502, 503, 504, 505, 506, 507, 508, 510, 511}

func newLogger() *logrus.Logger {
	logger = logrus.New()
	logger.Formatter = &logrus.JSONFormatter{}
	logger.Out = os.Stdout
	logger.Level = logrus.InfoLevel

	return logger
}

func TestStatusCodes(test *testing.T) {
	logger = newLogger()

	for _, statusCode := range statusCodes {
		name := fmt.Sprintf("Return %s", strconv.Itoa(statusCode))
		test.Run(name, func(t *testing.T) {
			runTest(test, statusCode)
		})
	}

}

func assertStatusCode(test testing.TB, got, want int) {
	test.Helper()
	if got != want {
		test.Errorf("handler returned unexpected response status code: got %d want %d", got, want)
	}
}

func assertStatusResponse(test testing.TB, got, want string) {
	test.Helper()
	if got != want {
		test.Errorf("handler returned unexpected response body: got %s want %s", got, want)
	}
}

func runTest(test testing.TB, status_code int) {
	test.Helper()

	request := httptest.NewRequest("GET", "/get_status_code", nil)
	request.Header.Set("status_code", strconv.Itoa(status_code))
	response := httptest.NewRecorder()
	handler := http.HandlerFunc(return_requested_status_code)
	handler.ServeHTTP(response, request)

	replyMessage, _ := json.Marshal(ReplyMessage{Message: http.StatusText(status_code)})
	assertStatusCode(test, response.Code, status_code)
	assertStatusResponse(test, response.Body.String(), string(replyMessage))
}
