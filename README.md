# status-robles-io

An http server written in Go that accepts a status code and returns that status code and message.

An instance of this program is hosted on GCP Cloud Run and is available at https://status.robles.io/status.
<br>

## What You'll Need

`docker`, `docker-compose`, and `make`.
<br>

## Usage
Launch a local instance of status-robles-io:

`make up`
<br>

Perform a GET request with no header and receive an informational message:

```
curl --include --url http://127.0.0.1:8081/status --request GET
HTTP/1.1 400 Bad Request
Content-Type: application/json
Date: Thu, 01 Jan 1970 00:00:00 GMT
Content-Length: 49

{"message":"Please provide a status_code header"}
```
<br>

Perform a GET request with a `status_code` header to get whatever status code you want. Any [valid status code](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes) should work:

```
curl --include --url http://localhost:8081/status --request GET --header "status_code: 200"
HTTP/1.1 200 OK
Content-Type: application/json
Date: Thu, 01 Jan 1970 00:00:00 GMT
Content-Length: 16

{"message":"OK"}
```

```
curl --include --url http://localhost:8081/status --request GET --header "status_code: 404"
HTTP/1.1 404 Not Found
Content-Type: application/json
Date: Thu, 01 Jan 1970 00:00:00 GMT
Content-Length: 23

{"message":"Not Found"}
```

```
curl --include --url http://localhost:8081/status --request GET --header "status_code: 502"
HTTP/1.1 502 Bad Gateway
Content-Type: application/json
Date: Thu, 01 Jan 1970 00:00:00 GMT
Content-Length: 25

{"message":"Bad Gateway"}
```
<br>

Run the basic tests located in main_test.go:

```
make test

=== RUN   TestStatusCodes
=== RUN   TestStatusCodes/Return_100
{"level":"info","msg":"Request received","time":"2022-06-03T20:54:00Z","uri":"/get_status_code"}
{"headers":{"status_code":["100"]},"level":"info","msg":"Headers received","time":"2022-06-03T20:54:00Z"}
{"level":"info","msg":"Status code requested","status_code":100,"time":"2022-06-03T20:54:00Z"}
=== RUN   TestStatusCodes/Return_101
{"level":"info","msg":"Request received","time":"2022-06-03T20:54:00Z","uri":"/get_status_code"}
{"headers":{"status_code":["101"]},"level":"info","msg":"Headers received","time":"2022-06-03T20:54:00Z"}
{"level":"info","msg":"Status code requested","status_code":101,"time":"2022-06-03T20:54:00Z"}

...

    --- PASS: TestStatusCodes/Return_510 (0.00s)
    --- PASS: TestStatusCodes/Return_511 (0.00s)
PASS
ok  	gitlab.com/carlosrobles/golang-http-status	0.017s

```
<br>

Stop and destroy the container

`make down`
